# dkim_verify

An educational project to take the requirements from RFC6376 and implement them in a Python script to verify DKIM signature of email messages.

**Requires**
- `git`
- `python3`
- `dnspython`
- `pycryptodome`
- `argparse`

**Installation**

It is highly recommended to use a Python virtual environment whenever possible.  First, clone this repo into a directory on your machine.

- `python3 -m venv venv-dir`
- `source venv-dir/bin/activate`
- `python3 -m pip install -r requirements`

**Usage**

`venv-dir/bin/python3 ./dkim_verify.py [-d] -f message.eml`

`-d` = debug mode

`-f message.eml` = message.eml contains the DKIM signed email message with the internet standard `\r\n` EOL convention.

**TODO**

There are a some features which are not yet implemented or implemented fully.
