#!/bin/python
# -*- coding: UTF-8 -*-

def debug_print(payload,breadcrumb):
    colour_blue = '\033[94m'
    colour_magenta = '\033[95m'
    colour_end = '\033[0m'
    if debug == True:
        print(f"{colour_magenta}--- BEGIN: {breadcrumb} ---{colour_end}")
        print(f"{colour_magenta}|{colour_blue} {payload} {colour_end}")
        print(f"{colour_magenta}--- END: {breadcrumb} ---{colour_end}")
    else:
        pass
    return

#----------------------------------------------------------------------
# Process the headers and create a nice list of headers (name:value)
def process_headers(headers: str) -> list:
    import re

    headers_list=[]
    # Unfold the headers
    headers_unfold = re.sub(b'\r\n[\t ]+',b' ',headers)
    result = re.findall(b'((?P<header_name>[^:]+): ?(?P<header_value>.+?)(\r\n|$))+?',headers_unfold)

    # Strip out the first entry of the tuple in each row of the array
    for header in result:
       key,value = header[1],header[2]
       headers_list.append([key,value])

    return headers_list

#----------------------------------------------------------------------
# Find the DKIM-Signature headers and pull out the body hash value (bh)
def extract_dkim_parameters(headers_list: list) -> list:
    import re
    found_dkim_signature=False
    dkim=[]
    for header in headers_list:
        raw_dkim={}
        if header[0] == b'DKIM-Signature':
          found_dkim_signature=True
          result = re.search(b"bh=([^;]+)(;|$)", header[1]).groups()
          raw_dkim[b'bh']=result[0]

          result = re.search(b"v=([^;]+)(;|$)", header[1]).groups()
          raw_dkim[b'v']=result[0]

          result = re.search(b"a=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b'a']=result[0]

          result = re.search(b"c=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b'c']=result[0]

          result = re.search(b"b=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b'b']=result[0]

          result = re.search(b"d=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b'd']=result[0]

          # THIS LINE NEEDS HELP MATCHING BEFORE THE H=
          result = re.search(b" h=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b'h']=result[0]

          result = re.search(b"i=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b'i']=result[0]

          result = re.search(b"l=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b'l']=result[0]

          result = re.search(b"q=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b'q']=result[0]

          result = re.search(b"s=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b's']=result[0]

          result = re.search(b"t=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b't']=result[0]

          result = re.search(b"x=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b'x']=result[0]

          result = re.search(b"z=([^;]+)(;|$)", header[1])
          if result != None:
              result = result.groups()
              raw_dkim[b'z']=result[0]

          dkim.append(raw_dkim)
    if found_dkim_signature == True:
        return dkim
    else:
        print("No DKIM-Signature found")
        exit(1)


#----------------------------------------------------------------------
# Take the raw email message and separate out the header from the body
def parse_message(message: str) -> str:
    import re

    # Split header section from body)
    headers,body = re.split(b'\r\n\r\n',message,maxsplit=1)

    return headers,body

#----------------------------------------------------------------------
# Compute the hash of message body
def compute_bh (body: str, algorithm: str, length: str) -> str:
    import base64
    import hashlib

    if algorithm == b'rsa-sha256':
        bh = base64.b64encode(hashlib.sha256(body).digest())
    elif algorithm == b'rsa-sha1':
        bh = base64.b64encode(hashlib.sha1(body).digest())
    else:
        #print(algorithm)
        print("Invalid algorithm: " + algorithm.decode())
        exit(1)

    return bh

#----------------------------------------------------------------------
# Compute the hash of message body
def canonicalize_body(body: str, body_canon_type: str) -> str:
    import re

    if body_canon_type == b'simple':
        debug_print(body_canon_type,'canonicalize_body() > body_canon_type')
        #### TODO - this might need tweaking as it's hard to test without examplars.
        # convert linefeed into carriage return + line feed
        #body = re.sub(b'\r?\n',b'\r\n',body)

        # remove whitespaces immediately before the end of line.
        #body = re.sub(b'[\t ]+\r\n',b'\r\n', body)

        # Remove extra tailing lines
        #body = re.sub(b'(\r\n)+$',b'\r\n', body)
    elif body_canon_type == b'relaxed':
        debug_print(body_canon_type, 'canonicalize_body() > body_canon_type')
        # convert linefeed into carriage return + line feed
        body = re.sub(b'\r?\n',b'\r\n',body)

        # remove whitespaces immediately before the end of line.
        body = re.sub(b'[\t ]+\r\n',b'\r\n', body)

        # collapse sequences of whitespaces to a single whitespace
        body = re.sub(b'[\t ]+',b' ',body)

        # Remove extra tailing lines
        body = re.sub(b'(\r\n)+$',b'\r\n', body)
    return body

#----------------------------------------------------------------------
# Compare the computed canonicalized body hash against the body hash from the DKIM-Signature
def compare_bh (dkim_bh: str, computed_bh: str) -> bool:
    if dkim_bh == computed_bh:
        result = True
    else:
        result = False
    return result

#----------------------------------------------------------------------
# Collect the domainkey DNS record.
def fetch_domainkey_record (selector: str, domain: str) -> str:
    from dns import resolver

    domainkey={}
    dns_record = str(selector.decode() + "._domainkey." + domain.decode())

    dns_response = resolver.resolve(dns_record,"TXT")

    if len(dns_response) == 1:
        for i in {'k','v','g','h','n','p','s','t'}:
            result = re.search(f'{i}=([^;]+)(;|$)',str(dns_response[0]))
            if result != None:
                domainkey[i] = result.groups()[0]
            else:
                domainkey[i] = None
    elif len(dns_response) == 0:
        print("No DNS domain key response found")
    else:
        print("Too many domain key responses received")

    return domainkey

#----------------------------------------------------------------------
# Canonicalized the message headers
def canonicalize_headers (headers_list: list, dkim_params: list, dkim_i: int, headers_canon_type: str) -> str:
    import re
    h_list=[]
    canon_headers=''
    dkim_signature=[]

    debug_print(headers_canon_type,'canonicalize_headers() > headers_canon_type')
    debug_print(dkim_params,'canonicalize_headers() > dkim_params')
    debug_print(headers_list,'canonicalize_headers() > headers_list')

    i=0
    while i <= len(headers_list)-1:
        if headers_list[i][0] == b'DKIM-Signature':
            dkim_signature.append([headers_list[i][0],headers_list[i][1]])
        i+=1

    if headers_canon_type == b'simple':
        debug_print(headers_canon_type,'canonicalize_headers() > headers_canon_type')
        #TODO This needs to be validated
        pass
    elif headers_canon_type == b'relaxed':
        debug_print(headers_canon_type,'canonicalize_headers() > headers_canon_type')
        h_list = re.split(b'[\t ]*:[\t ]*',dkim_params[b'h'])
        debug_print(h_list,'canonicalize_headers() > h_list')
        i=0
        while i <= len(h_list)-1:
            j=0
            while j <= len(headers_list)-1:
                if h_list[i].decode().lower() == headers_list[j][0].decode().lower():
                    # Strip leading whitespace on header values before appending them to the canon_headers
                    headers_list[j][1] = re.sub('^[\t ]+','',headers_list[j][1].decode()).encode()
                    canon_headers=canon_headers + headers_list[j][0].decode().lower() + ':' + headers_list[j][1].decode() + '\r\n'
                j+=1
            i+=1
        debug_print(dkim_signature[dkim_i][1],'canonicalize_headers() > dkim_signature[dkim_i][1]')
        dkim_signature[dkim_i][1] = re.sub(b' ?b=[^;]+',b' b=',dkim_signature[dkim_i][1])
        canon_headers = canon_headers + dkim_signature[dkim_i][0].decode().lower() + ":" + dkim_signature[dkim_i][1].decode()

        # remove whitespaces immediately before the end of line.
        canon_headers = re.sub('[\t ]+\r\n','\r\n', canon_headers)

        # collapse sequences of whitespaces to a single whitespace
        canon_headers = re.sub('[\t ]+',' ',canon_headers)
        debug_print(canon_headers,'canonicalize_headers() > canon_headers')

    return canon_headers

def verify_signature(domainkey: str, canon_headers: str, dkim_params: list) -> str:
    from Crypto.PublicKey import RSA
    from Crypto.Signature import PKCS1_v1_5
    from Crypto.Hash import SHA256
    from base64 import b64decode
    domainkey='-----BEGIN PUBLIC KEY-----\r\n' + domainkey + "\r\n-----END PUBLIC KEY-----\r\n"
    rsakey = RSA.import_key(domainkey)
    signer = PKCS1_v1_5.new(rsakey)
    digest = SHA256.new()
    debug_print(canon_headers,'verify_signature() > canon_headers')
    digest.update(canon_headers.encode())
    if signer.verify(digest, b64decode(dkim_params[b'b'])):
        return True
    return False

import sys
import re
import argparse

parser = argparse.ArgumentParser(description='DKIM Verification')
parser.add_argument('-d', help="Debugging mode", dest="debug", action="store_true")
parser.add_argument('-f', help="EML file", dest='eml_file')
args = parser.parse_args()

if args.debug == True:
    debug=True
    print("**** DEBUG MODE ENABLED ****")
else:
    debug=False

# Read the message file
f = open(args.eml_file, "rb")
message = f.read()
headers,body = parse_message(message)
headers_list = process_headers(headers)
dkim_params = extract_dkim_parameters(headers_list)

num_dkim_sigs = len(dkim_params)
i=0
while i < num_dkim_sigs:
    print("Checking DKIM parameters for: S = " + str(dkim_params[i][b's'].decode()) + " D = " + str(dkim_params[i][b'd'].decode()))
    algorithm = dkim_params[i][b'a']
    if b'l' in dkim_params[i].keys():
        length = dkim_params[i][b'l']
    else:
        length = None

    if b'c' in dkim_params[i].keys():
        headers_canon_type,body_canon_type = re.split(b'/',dkim_params[i][b'c'])
    else:
        print("No canonicalization type found.")
        exit(1)

    if b's' in dkim_params[i].keys():
        selector = dkim_params[i][b's']
    else:
        print("No selector found.")
        exit(1)

    if b'd' in dkim_params[i].keys():
        domain = dkim_params[i][b'd']
    else:
        print("No domain found.")
        exit(1)

    canon_body = canonicalize_body(body,body_canon_type)
    computed_bh = compute_bh(canon_body,algorithm,length)
    bh_match = compare_bh(dkim_params[i][b'bh'],computed_bh)
    colour_green='\033[92m'
    colour_red='\033[91m'
    colour_end='\033[0m'

    if bh_match == True:
        print(f"   {colour_green}PASS{colour_end}: Body hash match")
    else:
        print(f"   {colour_red}FAIL{colour_end}: Body hash don't match!")
        exit(1)

    domainkey = fetch_domainkey_record(selector, domain)
    canon_headers = canonicalize_headers(headers_list,dkim_params[i],i,headers_canon_type)
    debug_print(canon_headers, 'main() > canon_headers')
    # Check for a public key (p) in the domain key
    if domainkey['p'] != None:
        if verify_signature(domainkey['p'],canon_headers,dkim_params[i]) == True:
            print(f"   {colour_green}PASS{colour_end}: DKIM Signature verified")
        else:
            print(f"   {colour_red}FAIL{colour_end}: DKIM Signature not verified")
    else:
        print("No DKIM public key found in domainkey.  Cannot continue.")

    # This should be the last line in the loop
    i+=1
